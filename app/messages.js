const express = require('express');
const fs = require('fs');
const router = express.Router();
const path = "./messages";

router.post('/', (req, res) => {
  const datetime = new Date().toISOString();
  const requestBody = req.body;
  requestBody.dateTime = datetime;
  fs.writeFile(`${path}/${datetime}.txt`, JSON.stringify(requestBody), (err) => {
    if (err) {
      console.error(err);
    }
    console.log('File was saved!');
  });
  res.send(`${JSON.stringify(requestBody)}`);
});

getFiveLastFiles = () => {
  let paths = [];
  return new Promise((resolve, reject) => {
    fs.readdir(path, (err, files) => {
      if (err) {reject(err)}
      else {
        files.forEach(file => {
          paths.push(file)
        });
        let array = paths.slice(-5);
        resolve(array);
      }
    });
  });
};

getMessagesFromFiles = (array) => {
  let messages = [];
  array.forEach(file => {
    const promise = new Promise((resolve, reject) => {
      fs.readFile(`./messages/${file}`, "utf8", (err, data) => {
        if (err) {reject(err)}
        else {
          resolve(JSON.parse(data));
        }
      });
    });
    messages.push(promise);
  });
  return Promise.all(messages);
};

router.get('/', (req, res) => {
  getFiveLastFiles()
  .then(getMessagesFromFiles)
  .then(messages => {
    res.send(messages);
  });
});

module.exports = router;